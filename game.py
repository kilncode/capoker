from deuces import Card
from deuces import Deck
from deuces import Evaluator
import statistics

class Game(object):
    """
    The Game class exists to assist with automating many routine rounds of gameplay
    of a simple Texas Hold'em game leveraging the Deuces library: https://github.com/chrisking/deuces

    The goal is to support a few modes of gameplay such as:

    1. Only 2 known cards ( the player's pocket )
    2. Only 5 known cards ( pocket + flop )
    3. Only 6 known cards ( pocket + flop + turn )
    4. All 7 known cards ( pocket + flop + turn + river )

    From here we will also have the table size set as a parameter indicating the total
    number of participants at the table including the player.

    The last core input will be the number of iterations of gameplay desired. From this, the median percentage
    rate of success will be returned to the user to allow them to bet with confidence.

    The expected user interface to this class is a Jupyter Notebook, though it could be expanded to work well with bots.
    """

    def play_hands(self):
        """
        play_hands is a bit of a spaghetti mess right now, this runs all the iterations and deck management.

        Simulation, using the known cards, what is the evaluated rank of a hand?
        Perform that evaluation the number of times that we have iterations and then keep all the results in a list.
        The list can then be inspected to determine the best course of action by a player human or bot.

        :return: win_rates a list object of the estimated win rate of a given simulation.
        """
        stat_win_rates = []
        table_win_rates = []
        evaluator = Evaluator()

        for simulation in range(0, self.iterations):
            # Create a clean board for this simulation
            board = []
            # Generate a clean deck
            round_deck = Deck()
            # Remove Player's cards:
            round_deck = self.remove_player_cards_from_deck(deck=round_deck)
            # Process the board
            state = self.remove_board_from_deck(board=board, deck=round_deck)
            board = state['board']
            # generates an evaluation of the player's hand in the simulation
            p1_score = evaluator.evaluate(board, self.player_hand)
            p1_rank = 1.0 - evaluator.get_five_card_rank_percentage(p1_score)

            stat_win_rates.append(p1_rank * 100)
            table_win_rates.append(self.get_tablescores(board=board, p1_rank=p1_rank, other_hands=state['other_players']))
        # median is used in the notebook as interesting data point
        self.win_rate = statistics.median(stat_win_rates)
        return {'stat_win_rates': stat_win_rates, 'table_win_rates': table_win_rates}

    def get_tablescores(self, board, p1_rank, other_hands):
        """
        get_tablescores is designed to utilize the table size of play to determine a more
        accurate likelihood of winning based on the probability of someone having a better hand.

        The size of the length of other_hands is 1 - the table size ( 1 being the current active player ).
        In this function we will evaluate the player's hand against all of the other randomly generated hands,
        then determine what are the odds the player is winning.

        for every player:
            if active_rank > player_rank:
                score+1
        then
        score/players = win percentage
        Add this to a long running win_rate value.

        :param board: The active board
        :param other_hands: The collection of other hands based on table size
        :return: scores a list of the percentage of winning or losing.
        """
        evaluator =  Evaluator()
        table_score = 0.0

        for player in other_hands:
            player_score = evaluator.evaluate(board, player)
            player_rank = 1.0 - evaluator.get_five_card_rank_percentage(player_score)
            if p1_rank > player_rank:
                table_score += 1
        table_score = (table_score / (len(other_hands) + 1.0 ) * 100)
        return table_score

    def remove_board_from_deck(self, board, deck):
        """
        Exploratory function that will remove cards in the board from a deck.

        :param board: the currently active board
        :param deck: the currently active deck

        :return: Dictionary with the board and deck objects.
        """
        other_players = []
        if self.board:
            for card in self.board:
                # adds cards from the real game to the simulation
                board.append(card)
            for card in board:
                # remove all known cards from the deck
                deck.cards.remove(card)
            for seat in range(0, self.table_size - 1):
                # remove cards from the dec as if people are playing
                hand = deck.draw(2)
                other_players.append(hand)
            if len(board) < 5:
                # If the board is not complete, simulate padding it
                new_cards = deck.draw(5 - len(board))
                # This is done in case only 1 card is drawn
                if type(new_cards) is int:
                    deck.draw(1)
                    board.append(new_cards)
                else:
                    for card in new_cards:
                        deck.draw(1)
                        board.append(card)
        # If there is no known board, deal out the players first then create a board.
        else:
            for seat in range(0, self.table_size - 1):
                hand = deck.draw(2)
                other_players.append(hand)
            deck.draw(1)
            board = deck.draw(3)
            deck.draw(1)
            board.append(deck.draw(1))
            deck.draw(1)
            board.append(deck.draw(1))
        return {'board': board, 'deck': deck, 'other_players': other_players}

    def remove_player_cards_from_deck(self, deck):
        """
        remove_player_cards_from_deck is used to remove cards from the deck of this particular deck object.
        A bit less that useful now that we know we cannot rely on a new shuffle again. Perhaps it would be better
        to modify it to behave like the board function above.

        :param deck: the currently active deck
        :return: deck: the currently active deck
        """
        deck.cards.remove(self.player_hand[0])
        deck.cards.remove(self.player_hand[1])
        return deck

    def set_player_cards(self, card1, card2):
        """
        set_player_cards takes card strings and assigns them to cards, then removes them from the deck
        :param card1: String describing the first card ( Js = Jack of spades, Tc = Ten of clubs )
        :param card2: String describing the second card ( Js = Jack of spades, Tc = Ten of clubs )
        :return: None
        """
        self.player_hand.append(Card.new(card1))
        self.player_hand.append(Card.new(card2))

    def print_win_rate(self):
        """
        print_win_rate is a bit of a fluff function that spits out the median % of winning as a pretty number as well
        as the hand the player has
        :return: None
        """
        print("The likelihood of winning with hand: ")
        Card.print_pretty_cards(self.player_hand)
        rounded_rate = round(self.win_rate, 2)
        print(" is " + str(rounded_rate) + "%")

    def __init__(self):
        """
        Sets everything up.

        Important parameters:
        * board = current ative board
        * iterations = how many simulations you would like performed
        """
        self.player_hand = []
        self.deck = Deck()
        self.iterations = 10000
        self.board = []
        self.table_size = 8
        self.win_rate = 0.0