# Computer Assisted Poker

The goal of this repository is to build a system for providing guidance to poker players about what to do, purely from computer recognition of cards within the game.

To begin it will start with a notebook that manually manages the information at play, returning to the user simple odds about whether their hand will win given the number of players at the table.

To note the work has been shown to work with python 3.7.0.

This project does make use of the following external libraries that are not available via PIP:

* https://github.com/chrisking/deuces ( note the original author has not merged a PR that supports Python3 )

## Getting started:

Setup virtualenv / virtualenv wrapper with this https://swapps.com/blog/how-to-configure-virtualenvwrapper-with-python3-in-osx-mojave/ 
Use python3

Build a Virtualenv for capoker:

```bash
mkvirtualenv capoker
```

Make a project space and clone this repository:

```bash
mkdir -p ~/projects/
cd ~/projects
git clone git@bitbucket.org:kilncode/capoker.git
```

Also clone the deuces repo with the python3 fixes:

```bash
git clone git@github.com:chrisking/deuces.git
```

Next setup most of the dependencies for the project:

```bash
cd capoker
pip install -r requirements.txt
```

Install the deuces package from your local clone:

```bash
cd ~/projects/deuces
python setup.py install
```

Fix your local virtualenv to boot strap better:

```bash
vim ~/.virtualenvs/capoker/bin/postactivate
```

Add the following content to the file and save it, then exit:

```bash
#Python Stuffs
#Setting Up Paths
export PYTHONPATH=.:~/projects/capoker
cd ~/projects/capoker
```

Validate it by:

```bash
workon capoker
```

To operate it now launch jupyter lab:

```bash
jupyter lab
```

In the future to get started:

```bash
workon capoker
jupyter lab
```

The notebook to get started with the non-optimized version is `HoldemAssist.ipynb` if you have support concurrency use `HoldemAssist-MP.ipynb`

Concurrency is not supported on MacOS at this time.

In ubuntu raise the limits with:

```bash
ulimit -n 1000000
```

Then start the notebooks as normal.