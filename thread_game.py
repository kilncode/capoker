from deuces import Card
from deuces import Deck
from deuces import Evaluator
import multiprocessing
import numpy as np


from multiprocessing.context import Process
#Global Configs for multiprocessing
CPU_COUNT = multiprocessing.cpu_count()
# Using 2x due to HyperThreading
MAX_NUMBER_OF_PROCESSES = 2 * CPU_COUNT

def remove_board_from_deck(state_board, original_board, deck, table_size):
    """
    Exploratory function that will remove cards in the board from a deck.

    :param board: the currently active board
    :param deck: the currently active deck

    :return: Dictionary with the board and deck objects.
    """
    np.random.seed()
    np.random.shuffle(deck.cards)
    other_players = []
    if original_board:
        for card in original_board:
            # adds cards from the real game to the simulation
            state_board.append(card)
        for card in state_board:
            # remove all known cards from the deck
            deck.cards.remove(card)
        for seat in range(0, table_size - 1):
            # remove cards from the dec as if people are playing
            hand = deck.draw(2)
            other_players.append(hand)
        if len(state_board) < 5:
            # If the board is not complete, simulate padding it
            new_cards = deck.draw(5 - len(state_board))
            # This is done in case only 1 card is drawn
            if type(new_cards) is int:
                deck.draw(1)
                state_board.append(new_cards)
            else:
                for card in new_cards:
                    deck.draw(1)
                    state_board.append(card)
    # If there is no known board, deal out the players first then create a board.
    else:
        for seat in range(0, table_size - 1):
            hand = deck.draw(2)
            other_players.append(hand)
        deck.draw(1)
        state_board = deck.draw(3)
        deck.draw(1)
        state_board.append(deck.draw(1))
        deck.draw(1)
        state_board.append(deck.draw(1))
    return {'board': state_board, 'deck': deck, 'other_players': other_players}



def remove_player_cards_from_deck(deck, player_hand):
    """
    remove_player_cards_from_deck is used to remove cards from the deck of this particular deck object.
    A bit less that useful now that we know we cannot rely on a new shuffle again. Perhaps it would be better
    to modify it to behave like the board function above.

    :param deck: the currently active deck
    :return: deck: the currently active deck
    """
    deck.cards.remove(player_hand[0])
    deck.cards.remove(player_hand[1])
    return deck


def get_distributed_tablescores(player_hand, stat_win_rates, original_board,table_size, return_dict, simulation ):
    """
    Again pretty similar to get_tablescores but in this case we move the card removal process
    and deck manipulation into the get table scores function.

    We are also pausing here to evaluate the performance of this technique vs the original in performance


    :param board:
    :param p1_rank:
    :return:
    """

    # Remove the players card from a new deck object
    nd = Deck()
    round_deck = remove_player_cards_from_deck(deck=nd, player_hand=player_hand)

    # Generate the Evaluator:
    evaluator = Evaluator()
    table_score = 0.0
    board = []
    state = remove_board_from_deck(state_board=board, original_board=original_board, deck=round_deck,table_size=table_size)
    other_hands = state['other_players']
    board = state['board']
    p1_score = evaluator.evaluate(board, player_hand)
    p1_rank = 1.0 - evaluator.get_five_card_rank_percentage(p1_score)
    for player in other_hands:
        player_score = evaluator.evaluate(board, player)
        player_rank = 1.0 - evaluator.get_five_card_rank_percentage(player_score)
        if p1_rank > player_rank:
            table_score += 1
    table_score = (table_score / (len(other_hands) + 1.0) * 100)
    return_dict[simulation] =  {'table_score': table_score}
    return return_dict

class Game(object):
    """
    Fundamentally the exact same thing as game.py but with multiple threads to hopefully
    improve performance

    The Game class exists to assist with automating many routine rounds of gameplay
    of a simple Texas Hold'em game leveraging the Deuces library: https://github.com/chrisking/deuces

    The goal is to support a few modes of gameplay such as:

    1. Only 2 known cards ( the player's pocket )
    2. Only 5 known cards ( pocket + flop )
    3. Only 6 known cards ( pocket + flop + turn )
    4. All 7 known cards ( pocket + flop + turn + river )

    From here we will also have the table size set as a parameter indicating the total
    number of participants at the table including the player.

    The last core input will be the number of iterations of gameplay desired. From this, the median percentage
    rate of success will be returned to the user to allow them to bet with confidence.

    The expected user interface to this class is a Jupyter Notebook, though it could be expanded to work well with bots.
    """

    def play_hands(self):
        """
        play_hands is a bit of a spaghetti mess right now, this runs all the iterations and deck management.

        Simulation, using the known cards, what is the evaluated rank of a hand?
        Perform that evaluation the number of times that we have iterations and then keep all the results in a list.
        The list can then be inspected to determine the best course of action by a player human or bot.

        :return: win_rates a list object of the estimated win rate of a given simulation.
        """
        stat_win_rates = []
        table_win_rates = []

        processes = []
        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        for simulation in range(0, self.iterations):


            p = Process(target=get_distributed_tablescores, args=(self.player_hand, stat_win_rates,self.board,self.table_size, return_dict, simulation))
            processes.append(p)

        for p in processes:
            p.start()

        #table_win_rates.append(results['table_score'])
        # median is used in the notebook as interesting data point
        #self.win_rate = statistics.median(results['stat_win_rates'])
        table_win_rates = []
        # print(rates)
        for key, val in return_dict.items():
            table_win_rates.append(return_dict[key]['table_score'])
        return table_win_rates


    def get_tablescores(self, board, p1_rank, other_hands):
        """
        get_tablescores is designed to utilize the table size of play to determine a more
        accurate likelihood of winning based on the probability of someone having a better hand.

        The size of the length of other_hands is 1 - the table size ( 1 being the current active player ).
        In this function we will evaluate the player's hand against all of the other randomly generated hands,
        then determine what are the odds the player is winning.

        for every player:
            if active_rank > player_rank:
                score+1
        then
        score/players = win percentage
        Add this to a long running win_rate value.

        :param board: The active board
        :param other_hands: The collection of other hands based on table size
        :return: scores a list of the percentage of winning or losing.
        """
        evaluator =  Evaluator()
        table_score = 0.0

        for player in other_hands:
            player_score = evaluator.evaluate(board, player)
            player_rank = 1.0 - evaluator.get_five_card_rank_percentage(player_score)
            if p1_rank > player_rank:
                table_score += 1
        table_score = (table_score / (len(other_hands) + 1.0 ) * 100)
        return table_score

    def remove_board_from_deck(self, board, deck):
        """
        Exploratory function that will remove cards in the board from a deck.

        :param board: the currently active board
        :param deck: the currently active deck

        :return: Dictionary with the board and deck objects.
        """
        other_players = []
        if self.board:
            for card in self.board:
                # adds cards from the real game to the simulation
                board.append(card)
            for card in board:
                # remove all known cards from the deck
                deck.cards.remove(card)
            for seat in range(0, self.table_size - 1):
                # remove cards from the dec as if people are playing
                hand = deck.draw(2)
                other_players.append(hand)
            if len(board) < 5:
                # If the board is not complete, simulate padding it
                new_cards = deck.draw(5 - len(board))
                # This is done in case only 1 card is drawn
                if type(new_cards) is int:
                    deck.draw(1)
                    board.append(new_cards)
                else:
                    for card in new_cards:
                        deck.draw(1)
                        board.append(card)
        # If there is no known board, deal out the players first then create a board.
        else:
            for seat in range(0, self.table_size - 1):
                hand = deck.draw(2)
                other_players.append(hand)
            deck.draw(1)
            board = deck.draw(3)
            deck.draw(1)
            board.append(deck.draw(1))
            deck.draw(1)
            board.append(deck.draw(1))
        return {'board': board, 'deck': deck, 'other_players': other_players}

    def remove_player_cards_from_deck(self, deck):
        """
        remove_player_cards_from_deck is used to remove cards from the deck of this particular deck object.
        A bit less that useful now that we know we cannot rely on a new shuffle again. Perhaps it would be better
        to modify it to behave like the board function above.

        :param deck: the currently active deck
        :return: deck: the currently active deck
        """
        deck.cards.remove(self.player_hand[0])
        deck.cards.remove(self.player_hand[1])
        return deck

    def set_player_cards(self, card1, card2):
        """
        set_player_cards takes card strings and assigns them to cards, then removes them from the deck
        :param card1: String describing the first card ( Js = Jack of spades, Tc = Ten of clubs )
        :param card2: String describing the second card ( Js = Jack of spades, Tc = Ten of clubs )
        :return: None
        """
        self.player_hand.append(Card.new(card1))
        self.player_hand.append(Card.new(card2))

    def print_win_rate(self):
        """
        print_win_rate is a bit of a fluff function that spits out the median % of winning as a pretty number as well
        as the hand the player has
        :return: None
        """
        print("The likelihood of winning with hand: ")
        Card.print_pretty_cards(self.player_hand)
        rounded_rate = round(self.win_rate, 2)
        print(" is " + str(rounded_rate) + "%")

    def __init__(self):
        """
        Sets everything up.

        Important parameters:
        * board = current ative board
        * iterations = how many simulations you would like performed
        """
        self.player_hand = []
        self.deck = Deck()
        self.iterations = 10000
        self.board = []
        self.table_size = 8
        self.win_rate = 0.0